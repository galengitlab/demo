package com.example.demo;



/**
 * (Table)实体类
 *
 * @author makejava
 * @since 2023-09-26 21:31:27
 */
public class Table {

    private Long id;

    private Enum<mood> cm;

    private String xxx;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Enum<mood> getCm() {
        return cm;
    }

    public void setCm(Enum<mood> cm) {
        this.cm = cm;
    }

    public String getXxx() {
        return xxx;
    }

    public void setXxx(String xxx) {
        this.xxx = xxx;
    }

    @Override
    public String toString() {
        return "Table{" +
                "id=" + id +
                ", cm='" + cm + '\'' +
                ", xxx=" + xxx +
                '}';
    }
}

