package com.example.demo;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TableDao {

    Table query();
    Table queryById(Long id);
    int insert(Table table);
    int update(Table table);
    int deleteById();

}

