package com.example.demo;

public enum mood {

    sad(1,"sad"), ok(2,"ok"), happy(3,"happy");
    
    private int code;
    private String name;

    mood(int code, String name) {
        this.code=code;
        this.name=name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}